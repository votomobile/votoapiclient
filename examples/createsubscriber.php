<?php

require_once('../libraries/guzzle.phar');
require_once('../votoapiclass.php');

// Create a subscriber using the VOTO API!

// Enter your VOTO API key in the variable below:
$myApiKey = 'YOUR_API_KEY_HERE';

// Connect to the VOTO client and retrieve languages
$votoClient = New VotoApiClient($myApiKey, array('initLanguages' => 1));


// Retrieve the first language in the languages array
// For example purposes. Usually more logic here to handle multiple languages.
$defaultLanguage = array_values($votoClient->languages);
$defaultLanguage = $defaultLanguage[0]['id'];


$details = array(
	'phone' => '0230111111',
	'name' => 'Subscriber\'s name',
	'comments' => 'Added via the API. Rock on!',
	'receive_voice' => '1',
	'receive_sms' => '1',
	'starting_date' => '2013-10-10',
	'location' => 'Ghana',
	'active' => '1',
	'preferred_language' => $defaultLanguage,
	);

// Create a subscriber using the details above
$subscriberId = $votoClient->createSubscriber($details);

echo '<b>New subscriber created:</b>';
echo '<pre>';
print_r($votoClient->listSubscriberDetails($subscriberId));
echo '</pre>';

echo '<b>Logging:</b>';
echo '<pre>';
print_r($votoClient->logs);
echo '</pre>';
