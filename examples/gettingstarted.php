<?php

require_once('../libraries/guzzle.phar');
require_once('../votoapiclass.php');

// Example to get started with the VOTO client class library

// Enter your VOTO API key in the variable below:
$myApiKey = 'YOUR_API_KEY_HERE';

// Connect to the VOTO client and retrieve languages
$votoClient = New VotoApiClient($myApiKey, array('initLanguages' => 1));


echo '<b>Languages retrieved:</b>';
echo '<pre>';
print_r($votoClient->languages);
echo '</pre>';



echo '<b>Get subscribers:</b>';
echo '<pre>';
print_r($votoClient->listSubscribers());
echo '</pre>';


echo '<b>Logging:</b>';
echo '<pre>';
print_r($votoClient->logs);
echo '</pre>';