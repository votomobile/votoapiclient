# VOTO API Client

This library provides an easy-to-use set of PHP functions to access the VOTO API.

Full documentation for the API is here:

<https://go.votomobile.org/apidoc/>

The VOTO Mobile platform provides voice and SMS messaging and surveys through an interactive online platform. For more details visit <http://www.votomobile.org>.

## Issues

Comments, feedback, and bug reports are welcome and can be added to the [Issue Tracker](https://bitbucket.org/votomobile/votoapiclient/issues?status=new&status=open).

## License

This client library is open-source software licensed under the MIT License.

