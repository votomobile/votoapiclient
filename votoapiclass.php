<?php

// Sample VOTO API client class
// For the VOTO Mobile API:
// https://go.votomobile.org/apidoc/

// Version 0.1 (beta)
// 2014-03-27

// Re-use and edits are welcome!
// Licensed under the MIT License

// (c) 2014 VOTO Mobile
// http://www.votomobile.org/

// For questions or for API key access, email
// support@votomobile.org
// Note that this client library is provided as-is
// and results are not guaranteed!
// Feedback is welcome.

// Uses the Guzzle Library:
// http://guzzlephp.org/
// Many thanks to its developers.

class VotoApiClient {

	protected $api_key;
	protected $client;
	protected $arguments;

	public $languages;

	public $logs = array();

	const apiUrl = 'https://go.votomobile.org/api/v1/';
	

	public function __construct($api_key, $arguments = array()) {
		// Available arguments:
		// initLanguages -- retrieves the list of language ID's when loading the client, useful for connection testing and for operations that require language IDs (eg. creating surveys and messages).
		// indexArrays -- automatically index numeric arrays when these have a common structure with ID's.
		// skipSslVerification -- only used for internal testing purposes; will be deprecated in the near future. Not recommended.
		// overrideApiUrl -- only used for internal testing purposes; will be deprecated in the near future. Not recommended.

		$this->api_key = $api_key;

		$defaultArguments = array(
			'initLanguages' => 0,
			'indexArrays' => 1,
			'skipSslVerification' => 0,
			'overrideApiUrl' => '',
			);

		$this->arguments = array_merge($defaultArguments, $arguments);

		if($this->arguments['overrideApiUrl']) {
			$url = $this->arguments['overrideApiUrl'];
		}
		else {
			$url = self::apiUrl;
		}

		$this->client = new \Guzzle\Http\Client($url , array(
			'curl.options' => array(
			        'CURLOPT_SSLVERSION'	=> 3,
			    ),
			
			)
		);

		// Optionally initialize the languages array
		// Helpful as a test of your API key and network connections
		if($this->arguments['initLanguages']) {
			$this->languages = $this->listLanguages();
		}
		
	}

	// For reference on the functions below, see the associated documentation link.
	// All the best!


	// ////////////  Subscribers ////////////

	// Create subscriber
	// https://go.votomobile.org/apidoc/subscribers.html#create-subscriber
	public function createSubscriber($parameters) {

		/*
		// Example parameter array
		$parameters = array(
			'phone' => '',
			'receive_voice' => '',
			'receive_sms' => '',
			'starting_date' => '',
			'name' => '',
			'groups' => '',
			'location' => '',
			'comments' => '',
			'active' => '',
			'preferred_language' => '',
			);
			*/

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->post('subscribers', array(), $parameters);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// List subscribers
	// https://go.votomobile.org/apidoc/subscribers.html#list-subscribers
	public function listSubscribers() {
		return $this->getListContent('subscribers');
	}

	// List subscriber details
	// https://go.votomobile.org/apidoc/subscribers.html#list-subscriber-details
	public function listSubscriberDetails($subscriberId) {
		$request = $this->client->get('subscribers/'.$subscriberId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['subscriber'])) {
				return $output['subscriber'];
			}
		}
		return false;
	}

	// Modify subscriber details
	// https://go.votomobile.org/apidoc/subscribers.html#modify-subscriber-details
	public function modifySubscriberDetails($subscriberId, $parameters) {

		// Parameters: Specify only subscriber parameters to be changed

		/*
		// Example parameter array
		$parameters = array(
			'phone' => '',
			'receive_voice' => '',
			'receive_sms' => '',
			'starting_date' => '',
			'name' => '',
			'groups' => '',
			'location' => '',
			'comments' => '',
			'active' => '',
			'preferred_language' => '',
			);
			*/

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->put('subscribers/'.$subscriberId, array(), $parameters);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// Delete a subscriber
	// https://go.votomobile.org/apidoc/subscribers.html#delete-subscriber
	public function deleteSubscriber($subscriberId) {

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->delete('subscribers/'.$subscriberId);
		$request->getQuery()->add('api_key', $this->api_key);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;

	}




	// ////////////  Audio Files ////////////

	// Upload an audio file
	// https://go.votomobile.org/apidoc/audio_files.html#upload-an-audio-file
	// This function can also be used to update an existing audio file, by including the audio file ID as the last parameter.
	public function uploadAudioFile($filePath, $parameters = array(), $audioFileIdToUpdate = false) {

		// Optional parameters
		if(isset($parameters['description'])) {
			$description = $parameters['description'];
		}
		if(isset($parameters['languageId'])) {
			$languageId = $parameters['languageId'];
		}

		// File path -based information
		$pathinfo = pathinfo($filePath);

		$extension = $pathinfo['extension'];
		$fileName = $pathinfo['basename'];

		if(! $description) {
			$description = $fileName;
		}

		if($audioFileIdToUpdate) {
			// Then, we're updating an audio file instead of creating a new one.
			$request = $this->client->put('audio_files/'.$audioFileIdToUpdate);
		}
		else {
			// Otherwise, standard approach for new audio files.
			$request = $this->client->post('audio_files');
		}
		
		$request->setBody(fopen($filePath, 'r'));

		$request->getQuery()->add('api_key', $this->api_key);
		$request->getQuery()->add('description', $description);
		$request->getQuery()->add('file_extension', $extension);
		if($languageId) {
			$request->getQuery()->add('language_id', $languageId);
		}

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			// Returns an integer which is the ID of the audio file
			return $output;
		}
		return false;

	}

	// List audio files
	// https://go.votomobile.org/apidoc/audio_files.html#list-all-audio-files
	public function listAudioFiles() {
		return $this->getListContent('audio_files');
	}

	// List details of a single audio file
	// https://go.votomobile.org/apidoc/audio_files.html#list-details-of-an-audio-file
	public function listAudioFileDetails($audioFileId) {
		$request = $this->client->get('audio_files/'.$audioFileId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['audio_file'])) {
				$output = $output['audio_file'];
				return $output;
			}
		}
		return false;
	}

	// Delete an audio file
	// https://go.votomobile.org/apidoc/audio_files.html#delete-audio-file
	public function deleteAudioFile($audioFileId) {

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->delete('audio_files/'.$audioFileId);
		$request->getQuery()->add('api_key', $this->api_key);

		$response = $this->sendRequest($request);

		if($response) {
			// TODO - check for code 1009 ?
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;

	}

	// Download an audio file to the server
	// Retrieves the actual contents of the file from VOTO's online AWS storage.
	// https://go.votomobile.org/apidoc/audio_files.html#download-an-audio-file
	public function downloadAudioFile($audioFileId, $format = 'original') {
		// Format can also be, "ogg"
		$request = $this->client->get('downloads/audio_files/'.$audioFileId);
		$request->getQuery()->add('api_key', $this->api_key);
		$request->getQuery()->add('format', $format);
		$response = $this->sendRequest($request);

		if($response) {
			$this->addLog('Retrieved audio file '.$audioFileId.' of length '.$response->getHeader('Content-Length').' bytes.');
			// This is actually retrieved from AWS, after redirecting. Response format is different than normal (eg. is not a VOTO API JSON response).
			$output = $response->getBody();
			// TODO: review this. Currently sends it back as a PHP stream, which can be cast to a string (careful about high memory usage with larger files), or handled as a stream while passed to eg. a file write handler.

			// TODO: May be worth adding a file save path parameter to directly save output to disk.
			return $output;
			
		}
		else {
			$this->addLog('Could not download the requested audio file id ('.$audioFileId.').');
		}
		return false;
	}


	// ////////////  Messages ////////////

	// Create a new message
	// https://go.votomobile.org/apidoc/messages.html#create-a-message
	public function createMessage($parameters, $audioFiles = array(), $smsContent = array()) {

		/*
		// Example parameter array
		$parameters = array(
			'title' => '',
			'has_voice' => '',
			'has_sms' => '',
			);
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
			*/

		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_content['.$languageId.']'] = $smsMessage;
		}

		$request = $this->client->post('messages', array(), $parameters);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// Update a message
	// https://go.votomobile.org/apidoc/messages.html#update-a-message
	public function updateMessage($messageId, $parameters, $audioFiles = array(), $smsContent = array()) {

		/*
		// Example parameter array
		$parameters = array(
			'title' => '',
			'has_voice' => '',
			'has_sms' => '',
			);
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
			*/

		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_content['.$languageId.']'] = $smsMessage;
		}


		$request = $this->client->put('messages/'.$messageId, array(), $parameters);


		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// List Messages
	// https://go.votomobile.org/apidoc/messages.html#list-messages
	public function listMessages() {
		return $this->getListContent('messages');
	}

	// List details of a single message
	// https://go.votomobile.org/apidoc/messages.html#list-details-of-a-message
	public function listMessageDetails($messageId) {
		$request = $this->client->get('messages/'.$messageId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['message'])) {
				$output = $output['message'];
				if($this->arguments['indexArrays']) {
					// Index language-based values by language_id for convenience.
					// Since, for messages, there's only ever one value per language.
					$output['sms_content'] = $this->indexArray($output['sms_content'], 'language_id');
					$output['audio_files'] = $this->indexArray($output['audio_files'], 'language_id');
					// TODO - review if these should still include the empty values (without audio_file_id's and audio_file_descriptions). 
				}
				return $output;
			}
			
		}
		return false;
	}

	// Delete a message
	// https://go.votomobile.org/apidoc/messages.html#delete-a-message
	public function deleteMessage($messageId) {
		$parameters['api_key'] = $this->api_key;
		$request = $this->client->delete('messages/'.$messageId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;
	}



	// ////////////  Surveys ////////////

	// Create a new survey
	// https://go.votomobile.org/apidoc/surveys.html#create-a-survey
	public function createSurvey($parameters) {

		/*
		// Example parameter array
		$parameters = array(
			'survey_title' => '',
			'has_voice' => '',
			'has_sms' => '',
			'response_wait_time' => '',
			'question_repeat' => '',
			);
		*/
		// TODO: confirm if "survey_title" will change to "title"

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->post('surveys', array(), $parameters);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// Update a survey
	// (same parameters as above, but a put request with the $surveyId)
	// Documentation pending as of 2014-03-27
	public function updateSurvey($surveyId, $parameters) {

		/*
		// Example parameter array
		$parameters = array(
			'survey_title' => '',
			'has_voice' => '',
			'has_sms' => '',
			'response_wait_time' => '',
			'question_repeat' => '',
			);
		*/

		$parameters['api_key'] = $this->api_key;

		$request = $this->client->put('surveys/'.$surveyId, array(), $parameters);

		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;

	}

	// Add an introduction to a survey
	// https://go.votomobile.org/apidoc/surveys.html#add-a-survey-introduction
	public function addSurveyIntroduction($surveyId, $audioFiles = array(), $smsContent = array()) {
		return $this->addSurveyIntroductionOrConclusion($surveyId, 'introduction', $audioFiles, $smsContent);
	}

	// Add a conclusion to a survey
	// https://go.votomobile.org/apidoc/surveys.html#add-a-survey-conclusion
	public function addSurveyConclusion($surveyId, $audioFiles = array(), $smsContent = array()) {
		return $this->addSurveyIntroductionOrConclusion($surveyId, 'conclusion', $audioFiles, $smsContent);
	}

	// A shared function for both of the above
	// Since the procedure is very similar.
	private function addSurveyIntroductionOrConclusion($surveyId, $type, $audioFiles = array(), $smsContent = array()) {

		if($type != 'introduction' && $type != 'conclusion') {
			return false;
		}
		/*
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
			*/
		$parameters = array();
		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_'.$type.'['.$languageId.']'] = $smsMessage;
		}

		$request = $this->client->post('surveys/'.$surveyId.'/'.$type, array(), $parameters);


		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
			
		}
		return false;
	}		

	// Update an existing survey introduction
	// https://go.votomobile.org/apidoc/surveys.html#update-a-survey-introduction
	// Can use the last parameter (setting it to 0) to remove an introduction that exists.
	public function updateSurveyIntroduction($surveyId, $audioFiles = array(), $smsContent = array(), $hasIntroduction = null) {
		return $this->updateSurveyIntroductionOrConclusion($surveyId, 'introduction', $audioFiles, $smsContent, $hasIntroduction);

	}

	// Update an existing survey conclusion
	// https://go.votomobile.org/apidoc/surveys.html#update-a-survey-conclusion
	// Same as above - can remove an existing conclusion by setting the last parameter to 0.
	public function updateSurveyConclusion($surveyId, $audioFiles = array(), $smsContent = array(), $hasConclusion = null) {
		return $this->updateSurveyIntroductionOrConclusion($surveyId, 'conclusion', $audioFiles, $smsContent, $hasConclusion);

	}

	// Shared common function for updating both introductions and conclusions.
	private function updateSurveyIntroductionOrConclusion($surveyId, $type, $audioFiles = array(), $smsContent = array(), $hasIntroductionOrConclusion = null) {

		if($type != 'introduction' && $type != 'conclusion') {
			return false;
		}

		/*
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
			*/
		$parameters = array();
		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_'.$type.'['.$languageId.']'] = $smsMessage;
		}

		// Due to the existing setup of the API, if has_introduction or has_conclusion is explicitly set, then the rest of the updates are ignored. Hence, null for these values.
		if($hasIntroductionOrConclusion != null) {
			if($hasIntroductionOrConclusion == 1) {
				$parameters['has_'.$type] = '1';
			}
			elseif($hasIntroductionOrConclusion == 0) {
				$parameters['has_'.$type] = '0';
			}
		}
		

		$request = $this->client->put('surveys/'.$surveyId.'/'.$type, array(), $parameters);


		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			// TODO: review this pending API fix.
			return $output;
		}
		return false;

	}

	// Get survey introduction details
	// https://go.votomobile.org/apidoc/surveys.html#get-survey-introduction-details
	public function getSurveyIntroduction($surveyId) {
		return $this->getSurveyIntroductionOrConclusion($surveyId, 'introduction');
	}

	// Get survey conclusion details
	// https://go.votomobile.org/apidoc/surveys.html#get-survey-conclusion-details
	public function getSurveyConclusion($surveyId) {
		return $this->getSurveyIntroductionOrConclusion($surveyId, 'conclusion');
	}

	// Shared function for both introductions and conclusions
	private function getSurveyIntroductionOrConclusion($surveyId, $type) {

		if($type != 'introduction' && $type != 'conclusion') {
			return false;
		}
		$request = $this->client->get('surveys/'.$surveyId.'/'.$type);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['survey_'.$type])) {
				$output = $output['survey_'.$type];
				if($this->arguments['indexArrays']) {
					// Index language-based values by language_id for convenience.
					// Since, for messages, there's only ever one value per language.
					$output['sms_content'] = $this->indexArray($output['sms_content'], 'language_id');
					$output['audio_files'] = $this->indexArray($output['audio_files'], 'language_id');
					// TODO - review if these should still include the empty values (without audio_file_id's and audio_file_descriptions). 
				}
				return $output;
			}
			
		}
		return false;
	}

	// Create a survey question
	// https://go.votomobile.org/apidoc/surveys.html#create-a-survey-question
	public function addSurveyQuestion($surveyId, $parameters, $audioFiles = array(), $smsContent = array(), $options = array(), $conditions = array()) {

		/*
		// Example parameter array
		$parameters = array(
			'question_title' => '',
			'response_type' => '',
			'max_numeric_digits' => '',
			'max_open_length' => '',
			);
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
		// Conditions, in the following format
		$smsContent = array(
			0 => '1,conclude',
			1 => '2,end_call',
			2 => '3,25',
			3 => 'any_option,223',
			);
			*/
		
		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_content['.$languageId.']'] = $smsMessage;
		}
		foreach($options as $option) {
			$parameters['options'][] = $option;
		}
		foreach($conditions as $condition) {
			$parameters['condition'][] = $option;
		}

		$request = $this->client->post('surveys/'.$surveyId.'/questions', array(), $parameters);


		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
			
		}
		return false;
	}

	// Update a survey question
	// https://go.votomobile.org/apidoc/surveys.html#update-a-survey-question
	public function updateSurveyQuestion($surveyId, $questionId, $parameters, $audioFiles = array(), $smsContent = array(), $options = array(), $conditions = array()) {

		/*
		// Example parameter array
		$parameters = array(
			'question_title' => '',
			'response_type' => '',
			'max_numeric_digits' => '',
			'max_open_length' => '',
			);
		// Example audioFiles array
		$audioFiles = array(
			'23' => '102',
			);
		// which in practice is,
		$audioFiles = array(
			$language_id => $audio_file_id,
			);
		// Same for smsContent, for example:
		$smsContent = array(
			'39' => 'Hello there.',
			'40' => 'Ete sen.',
			'41' => 'Bonjour.',
			);
			*/
		
		$parameters['api_key'] = $this->api_key;

		foreach($audioFiles as $languageId => $audioFileId) {
			$parameters['audio_file['.$languageId.']'] = $audioFileId;
		}
		foreach($smsContent as $languageId => $smsMessage) {
			$parameters['sms_content['.$languageId.']'] = $smsMessage;
		}
		foreach($options as $option) {
			$parameters['options'][] = $option;
		}
		foreach($conditions as $condition) {
			$parameters['condition'][] = $option;
		}

		$request = $this->client->put('surveys/'.$surveyId.'/questions/'.$questionId, array(), $parameters);


		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
			
		}
		return false;
	}		

	// List all surveys
	// https://go.votomobile.org/apidoc/surveys.html#list-all-surveys
	public function listSurveys() {
		return $this->getListContent('surveys');
	}

	// List details of a survey
	// https://go.votomobile.org/apidoc/surveys.html#list-survey-details
	public function listSurveyDetails($surveyId) {
		$request = $this->client->get('surveys/'.$surveyId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['survey'])) {
				$output = $output['survey'];
				return $output;
			}
			
		}
		return false;
	}

	// List questions for a given survey
	// https://go.votomobile.org/apidoc/surveys.html#list-survey-questions
	public function listSurveyQuestions($surveyId) {
		$request = $this->client->get('surveys/'.$surveyId.'/questions');
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['questions'])) {
				$questions = $output['questions'];
				if($this->arguments['indexArrays']) {
					
					foreach($questions as &$question) {
						$question['sms_content'] = $this->indexArray($question['sms_content'], 'language_id');
						$question['audio_files'] = $this->indexArray($question['audio_files'], 'language_id');
						// TODO - review if these should still include the empty values (without audio_file_id's and audio_file_descriptions). 
					}
					
				}
				return $questions;
			}
			
		}
		return false;
	}

	// Get details for a specific question in a survey
	// https://go.votomobile.org/apidoc/surveys.html#get-survey-question-details
	public function getSurveyQuestionDetails($surveyId, $questionId) {
		$request = $this->client->get('surveys/'.$surveyId.'/questions/'.$questionId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['question'])) {
				$output = $output['question'];
				if($this->arguments['indexArrays']) {
					
					$output['sms_content'] = $this->indexArray($output['sms_content'], 'language_id');
					$output['audio_files'] = $this->indexArray($output['audio_files'], 'language_id');
					// TODO - review if these should still include the empty values (without audio_file_id's and audio_file_descriptions). 
					
					
				}
				return $output;
			}
			
		}
		return false;
	}

	// Delete a question from a survey
	// https://go.votomobile.org/apidoc/surveys.html#delete-a-question
	public function deleteSurveyQuestion($surveyId, $questionId) {
		$parameters['api_key'] = $this->api_key;
		$request = $this->client->delete('surveys/'.$surveyId.'/questions/'.$questionId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;
	}

	// Delete a whole survey
	// (use with caution!)
	// https://go.votomobile.org/apidoc/surveys.html#delete-a-survey
	public function deleteSurvey($surveyId) {
		$parameters['api_key'] = $this->api_key;
		$request = $this->client->delete('surveys/'.$surveyId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;
	}


	// ////////////  Groups ////////////
	
	// Create a group
	// https://go.votomobile.org/apidoc/groups.html#create-groups
	public function createGroup($parameters) {
		/*
		// Example parameter array
		$parameters = array(
		'name' => 'Api create group testing',
		'description' => 'created from the api library',
		);
		*/

		$parameters['api_key'] = $this->api_key;
		$request = $this->client->post('groups', array(), $parameters);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;
	}

	// Modify group details
	// https://go.votomobile.org/apidoc/groups.html#modify-group-details
	public function updateGroup($groupId, $parameters) {
		return $this->modifyGroupDetails($groupId, $parameters);
	}

	public function modifyGroupDetails($groupId, $parameters) {
		// Parameters: Specify gruop parameters to be changed only

		/*
		// Example parameter array
		$parameters = array(
		'name' => 'Api create group testing',
		'description' => 'created from the api library',
		);
		*/

		$parameters['api_key'] = $this->api_key;
		$request = $this->client->put('groups/'.$groupId, array(), $parameters);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;
	}

	// List groups
	// https://go.votomobile.org/apidoc/groups.html#list-groups
	public function listGroups() {
		return $this->getListContent('groups');
	}

	// List group details
	// https://go.votomobile.org/apidoc/groups.html#list-group-details
	// (Note that this only includes the name and description - not included subscribers)
	public function listGroupDetails($groupId) {
		$request = $this->client->get('groups/'.$groupId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['group'])) {
				$output = $output['group'];
				return $output;
			}
			
		}
		return false;
	}

	// Delete a group
	// https://go.votomobile.org/apidoc/groups.html#delete-group
	public function deleteGroup($groupId) {
		$parameters['api_key'] = $this->api_key;
		$request = $this->client->delete('groups/'.$groupId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return true;
		}
		return false;
	}


	// ////////////  Outgoing Calls ////////////

	// Helper functions for various sets of intended subscibers
	// eg. send to group, send to an array of subscribers, or send to one or more phone numbers.
	// Details for parameters in the createOutgoingCall(...) function below
	public function sendCallToGroups($parameters, $groupsArray, $scheduleParameters = array()) {
		$parameters['groups'] = implode(',', $groupsArray);
		return $this->createOutgoingCall($parameters, $scheduleParameters);
	}
	public function sendCallToPhoneNumbers($parameters, $phoneNumberArray, $scheduleParameters = array()) {
		$parameters['send_to_phones'] = implode(',', $phoneNumberArray);
		return $this->createOutgoingCall($parameters, $scheduleParameters);
	}
	public function sendCallToSubscribers($parameters, $subscribersArray, $scheduleParameters = array()) {
		$parameters['send_to_subscribers'] = implode(',', $subscribersArray);
		return $this->createOutgoingCall($parameters, $scheduleParameters);
	}

	// Create an outgoing call
	// https://go.votomobile.org/apidoc/outgoing_calls.html#create-an-outgoing-call
	public function createOutgoingCall($parameters, $scheduleParameters = array()) {

		/*
		// Example parameter array
		$parameters = array(
			'message_id' => '',
			'survey_id' => '',
			'groups' => '',
			'send_to_phones' => '',
			'send_to_subscribers' => '',

			'retry_attempts_long' => '',
			'retry_delay_long' => '',
			'retry_attempts_short' => '',
		);
		$scheduleDetails = array(
			'schedule_type' => '',

			'schedule_date' => '',
			'schedule_time' => '',
			'routine_days' => '',
			'routine_time' => '',

			'repeat_every' => '',
			'repeat_start_date' => '',
			'repeat_end_type' => '',
		);
		*/

		// if(! $scheduleParameters) {
		// 	$scheduleParameters['schedule_type'] = 'now';
		// }
		$parameters = array_merge($parameters, $scheduleParameters);

		$parameters['api_key'] = $this->api_key;
		$request = $this->client->post('outgoing_calls', array(), $parameters);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;
	}

	// List outgoing calls
	// https://go.votomobile.org/apidoc/outgoing_calls.html#list-outgoing-calls
	public function listOutgoingCalls() {
		return $this->getListContent('outgoing_calls');
	}

	// List details for a specific outgoing call
	// https://go.votomobile.org/apidoc/outgoing_calls.html#list-details-of-an-outgoing-call
	public function listOutgoingCallDetails($outgoingCallId) {
		$request = $this->client->get('outgoing_calls/'.$outgoingCallId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);
		
		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['outgoing_call'])) {
				$output = $output['outgoing_call'];
				return $output;
			}
		}
		return false;
	}


	// ////////////  Outgoing Calls ////////////

	// Create a new language for your organization
	// https://go.votomobile.org/apidoc/languages.html#create-languages
	public function createLanguage($parameters) {
		/*
			// Example parameter array
			$parameters = array(
			'language_name' => 'Swahili',
			'languange_abbreviation' => 'SW',
			);
		*/

		$parameters['api_key'] = $this->api_key;
		$request = $this->client->post('languages', array(), $parameters);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;
	}

	// List Languages
	// https://go.votomobile.org/apidoc/languages.html#list-languages
	public function listLanguages() {
		return $this->getListContent('languages');
	}

	// List a single language
	// https://go.votomobile.org/apidoc/languages.html#list-language-details
	public function listLanguageDetails($languageId) {
		$request = $this->client->get('languages/'.$languageId);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			if(isset($output['language'])) {
				$output = $output['language'];
				return $output;
			}
		}
		return false;
	}

	// Modify details for a specific language
	// https://go.votomobile.org/apidoc/languages.html#modify-language-details
	public  function modifyLanguage($languageId, $parameters) {
		/*
		// Example parameter array
		$parameters = array(
			'language_name' => 'Spanish',
			'language_abbreviation' => 'SP',
		);
		*/

		$parameters['api_key'] = $this->api_key;
		$request = $this->client->put('languages/'.$languageId, array(), $parameters);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
		}
		return false;	
	}

	// Delete a language
	// This is super dangerous and deletes all associated data
	// https://go.votomobile.org/apidoc/languages.html#delete-language
	public function deleteLanguage($languageId, $confirm = 0) {
		// Because deleting a language is so destructive (eg. deletes all associated surveys, messages, results, etc.!)
		// We've added a confirmation value here. :)
		if($confirm) {
			$parameters['api_key'] = $this->api_key;
			$request = $this->client->delete('languages/'.$languageId);
			$request->getQuery()->add('api_key', $this->api_key);
			$response = $this->sendRequest($request);

			if($response) {
				$output = $this->decodeResponse($response->getBody());
				return true;
			}
		}
		return false;
	}



	// ////////////  Delivery Logs ////////////
	
	// Get details of calls sent to a specific subscriber
	// https://go.votomobile.org/apidoc/delivery_logs.html#get-details-of-calls-sent-to-a-subscriber
	public function getDeliveryLogs($subscriberId) {
		$request = $this->client->get('subscribers/'.$subscriberId.'/delivery_logs');
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);
		
		if($response) {

			$output = $this->decodeResponse($response->getBody());
			// No key in this case - just array directly.
			return $output;
		}
		return false;
	}

	// Call Summary for an Outgoing Call
	// https://go.votomobile.org/apidoc/delivery_logs.html#call-summary-for-an-outgoing-call
	public function getOutgoingCallSummary($outgoingCallId) {
		// Same as the listOutgoingCallDetails(... )function
		return $this->listOutgoingCallDetails($outgoingCallId);
	}



	// ////////////  Results and Responses ////////////

	// Get survey responses
	// https://go.votomobile.org/apidoc/results.html#survey-responses-results
	public function getSurveyResponses($surveyId, $pollIdentifier = null) {
		$request = $this->client->get('surveys/'.$surveyId.'/results');
		$request->getQuery()->add('api_key', $this->api_key);

		if($pollIdentifier) {
			$request->getQuery()->add('poll_identifier', $pollIdentifier);
		}

		$response = $this->sendRequest($request);
		
		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
			
		}
		return false;
	}

	// Get survey responses for a specific question
	// https://go.votomobile.org/apidoc/results.html#get-survey-question-results-responses
	public function getSurveyQuestionResponses($surveyId, $questionId, $pollIdentifier = null) {
		$request = $this->client->get('surveys/'.$surveyId.'/questions/'.$questionId.'/results');
		$request->getQuery()->add('api_key', $this->api_key);

		if($pollIdentifier) {
			$request->getQuery()->add('poll_identifier', $pollIdentifier);
		}
		
		$response = $this->sendRequest($request);
		
		if($response) {
			$output = $this->decodeResponse($response->getBody());
			return $output;
			
		}
		return false;
	}



	// ////////////  Miscellaneous helper functions ////////////

	// Re-usable function for standard list GET requests
	// Since these all follow a similar structure
	private function getListContent($contentSource) {

		$request = $this->client->get($contentSource);
		$request->getQuery()->add('api_key', $this->api_key);
		$response = $this->sendRequest($request);

		if($response) {
			$output = $this->decodeResponse($response->getBody());

			$outputData = $output[$contentSource]; // eg. $output['languages'] or $output['subscribers']

			if($this->arguments['indexArrays']) {
				$outputData = $this->indexArray($outputData);
			}

			return $outputData;

			
		}
		else {
			return false;
		}
		

	}


	// Decode the standard JSON response returned by the API
	// Provides some logging to help interpret responses.
	private function decodeResponse($input) {

		
		$output = json_decode($input, 1);
		if(isset($output['data']) && $output['data']) {
			if(! $output['message']) {
				$output['message'] = '[empty message]';
			}
			$this->addLog($output['code'].': '.$output['message']);
			return $output['data'];
		}
		elseif(isset($output['code']) && isset($output['message'])) {
			$this->addLog($output['code'].': '.$output['message']);
		}
	

		return false;

	}

	// Send a Guzzle-based CURL request
	// Shared function to provide error handling.
	// For reference,
	// // http://guzzle.readthedocs.org/en/latest/http-client/request.html#working-with-errors
	private function sendRequest($request) {
		try {
			if($this->arguments['skipSslVerification']) {
				// For testing purposes only: 
				// http://inchoo.net/tools-frameworks/symfony2-guzzle-ssl-self-signed-certificate/
				$request->getCurlOptions()->set(CURLOPT_SSL_VERIFYHOST, false);
				$request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, false);
			}
			$response = $request->send();
			return $response;
		}
		catch (Guzzle\Http\Exception\BadResponseException $e) {
			$this->addLog('HTTP exception: '.$e->getMessage());
		}
		catch(Guzzle\Http\Exception\CurlException $e) {
			$this->addLog('Curl exception: '.$e->getMessage());
		}
		catch (Exception $ex) {
			$this->addLog('Other connection exception. Check your API key and network connection.');
		}
		return false;
	}

	// Index numeric arrays if they have a common "id" key
	// Useful for both numeric lists (eg. of subscribers, groups, etc.)
	// Or for values that all have language_id's that can be indexed.
	private function indexArray($array, $key = 'id') {
		$outputArray = array();

		foreach($array as $item) {
			if(isset($item[$key])) {
				$outputArray[$item[$key]] = $item;
			}
			else {
				$outputArray[] = $item;
			}
		}

		return $outputArray;



	}


	// Helper logging
	// Because logs are great!
	private function addLog($text) {
		$this->logs[] = '['.date('Y-m-d H:i:s').'] '.$text;
	}

	public function getLogs() {
		return $this->logs;
	}

	// Thanks for reading!
	// For questions email support@votomobile.org
	// Rock on!


}