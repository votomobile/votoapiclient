<?php

require_once('../libraries/guzzle.phar');
require_once('../votoapiclass.php');

// Example for uploading audio with the VOTO API
// Add your API key below, and change the filename on line 19 to a file on your system.

// Enter your VOTO API key in the variable below:
$myApiKey = 'YOUR_API_KEY_HERE';

// Connect to the VOTO client
// Here, we can skip the automatic languages retrieval (visible in the other examples).
$votoClient = New VotoApiClient($myApiKey);


// Upload an audio file
// Change this path to one where you have a wav, mp3, or aac file!
$file = dirname(__FILE__).'/../audiofiles/YOUR_FILENAME_HERE.wav';


if(file_exists($file)) {
	// Upload the file! This may take a few minutes 
	// depending on the filesize + network connection.
	$audioFileId = $votoClient->uploadAudioFile($file);
}
else {
	echo 'Could not locate file - check the file path and try again.';
}

// You can confirm that the audio file is uploaded (and play it back) from
// https://go.votomobile.org/audio/library (after signing in to the app)

if($audioFileId) {
	// Audio uploaded successfully!
	// Retrieve the details using that ID:
	echo '<b>New audio file uploaded:</b>';
	echo '<pre>';
	print_r($votoClient->listAudioFileDetails($audioFileId));
	echo '</pre>';
}
else {
	// No luck uploading.
	// May sometimes fail depending on the reliability of your network connection
	echo '<b>Sorry. Failed to upload audio file.</b><br/>';
}



echo '<b>Logging:</b>';
echo '<pre>';
print_r($votoClient->logs);
echo '</pre>';
